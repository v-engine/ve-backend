# VisualEngine

_Back-end_

For further information and for a demo of the project visit [v-engine.gitlab.io](https://v-engine.gitlab.io).

## Requirements

- mongoDB
- Python 3.6.0
- `virtualenv`

## Installing and running

To run the server

1.  `$ ./init.sh` - Required only the first time, it creates the virtualenv and it installs all packages
2.  `$ ./startdb.sh` - Personalize it with your mongo directory or don't run it if `mongod` daemon is already running
3.  `$ source env/bin/activate` - For enabling the virtual env
4.  `$ ./scrape.sh` - For scraping pages (with scrapy), populating db and creating the necessary data structures (by using `post-process.py` script) [also see sample db section below]
5.  `$ ./start.sh` - For starting the backend server

## Starting the crawler

To start the crawler

1.  `$ cd crawler`
2.  `$ scrapy crawl SPIDER_NAME` - Available spiders: `engcompsci`, `medium`

## Sample DB

You can download the sample database used for the paper and the presentation [here](https://mega.nz/#!GUtziIRY!0-2k8bUk39s3o2gRYTKGlO6xiY3YSuKZK0lRcpdBWzQ). It has about 15'000 pages and it was created by starting the crawler with [Jaguar (disambiguation)](<https://en.wikipedia.org/wiki/Jaguar_(disambiguation)>) and [Quantum (disambiguation)](<https://en.wikipedia.org/wiki/Quantum_(disambiguation)>) pages. The database is ready to go, the script `post-process.py` it is not needed.

For restoring:

1. Start mongodb daemon
2. Decompress the archive
3. `mongorestore -d "v-engine" v-engine/binary-data.bson`
4. `mongorestore -d "v-engine" v-engine/pages.bson`
5. Restart the backend
