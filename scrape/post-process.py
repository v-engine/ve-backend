import pymongo
import pickle
from bson.binary import Binary
from crawler.crawler.settings import *
from math import log

from nltk.tokenize import word_tokenize
from sklearn.feature_extraction.text import TfidfVectorizer
from sklearn.decomposition import TruncatedSVD
from sklearn.pipeline import make_pipeline
from sklearn.preprocessing import Normalizer


class Statistics:
    def __init__(self):
        connection = pymongo.MongoClient(
            MONGODB_SERVER,
            MONGODB_PORT
        )
        db = connection[MONGODB_DB]
        self.pages_collection = db[MONGODB_PAGES_COLLECTION]
        self.bin_data_collection = db[MONGODB_BIN_DATA_COLLECTION]
        # matrices
        self.tf_idf_matrix = []
        # init
        self.computeTfIdfMatrix()
        self.insertAutoincrementIndex()
        self.setTrueLinks()

    def computeTfIdfMatrix(self):
        print('> Computing Tf-Idf matrix')
        documents = self.pages_collection.find().sort('_id', pymongo.ASCENDING)
        docs = []
        for doc in documents:
            doc = doc['text']
            docs.append(doc)
        tf_idf_model = TfidfVectorizer(tokenizer=word_tokenize, stop_words='english')
        self.tf_idf_matrix = tf_idf_model.fit_transform(docs)

        print('> Storing Tf-Idf matrix in DB')

        self.bin_data_collection.update_one(
            {
                'name': 'tf_idf_matrix',
            },
            {
                "$set": {
                    'data': Binary(pickle.dumps(self.tf_idf_matrix, pickle.HIGHEST_PROTOCOL))
                }
            },
            upsert=True)

        print('> Storing Tf-Idf model with documents in DB')

        self.bin_data_collection.update_one(
            {
                'name': 'tf_idf_model',
            },
            {
                "$set": {
                    'data': Binary(pickle.dumps(tf_idf_model, pickle.HIGHEST_PROTOCOL))
                }
            },
            upsert=True)

    def insertAutoincrementIndex(self):
        print("> Creating indices", end="")
        documents = self.pages_collection.find().sort('_id', pymongo.ASCENDING)
        index = 0
        for index in range(documents.count()):
            print("\r> Creating indices... " + str(index) + "/" + str(documents.count()), end="")
            self.pages_collection.update_one(
                {
                    '_id': documents[index]['_id']
                },
                {
                    "$set": {
                        'index': index
                    }
                },
                upsert=True
            )
        print()

    def setTrueLinks(self):
        print("> Setting true links...", end="")
        documents = self.pages_collection.find().sort('_id', pymongo.ASCENDING)

        url_hashes = []
        for page in documents:
            url_hashes.append(page['url_hash'])  # here we have the url_hash of our pages

        documents = self.pages_collection.find().sort('_id', pymongo.ASCENDING)
        i = 0
        for page in documents:
            i += 1
            print("\r> Setting true links... " + str(i) + "/" + str(documents.count()), end="")
            true_links = []
            for link in page['links']['data']:
                if((link in url_hashes) and (link not in true_links) and (link != page['url_hash'])):
                    true_links.append(link)

            self.pages_collection.update_one(
                {
                    '_id': page['_id']
                },
                {
                    "$set": {
                        'links': {
                            'count': len(true_links),
                            'data': true_links
                        }
                    }
                },
                upsert=True
            )
        print()


def main():
    stat = Statistics()


main()
