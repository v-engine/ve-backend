from scrapy.spiders import CrawlSpider, Rule
from scrapy.linkextractors import LinkExtractor
from crawler.items import CrawlerItem
import hashlib
import re
from SPARQLWrapper import SPARQLWrapper, JSON


from bs4 import BeautifulSoup


class EngcompsciSpider(CrawlSpider):
    name = 'engcompsci'
    allowed_domains = ["en.wikipedia.org"]
    start_urls = [
        'https://en.wikipedia.org/wiki/Jaguar_(disambiguation)',
        'https://en.wikipedia.org/wiki/Quantum_(disambiguation)'
    ]

    rules = (
        Rule(LinkExtractor(
            allow=('https://en.wikipedia.org/wiki/.'),
            allow_domains=('en.wikipedia.org')
        ),
            callback="parse_item",
            follow=True),
    )

    custom_settings = {
        "DEPTH_PRIORITY": 1,
        "SCHEDULER_DISK_QUEUE": 'scrapy.squeues.PickleFifoDiskQueue',
        "SCHEDULER_MEMORY_QUEUE": 'scrapy.squeues.FifoMemoryQueue'
    }

    def clean_response(self, response):
        'Clean here the response and return the item with the clean text'
        soup = BeautifulSoup(response.body, "html5lib")
        for tag in soup(['script', 'style']):
            tag.decompose()
        # text = soup.get_text()
        # get the first paragraph
        paragraphs = soup.find(
            'div', {"class": "mw-parser-output"}).find_all('p')
        text = ""
        for i in range(len(paragraphs)):
            text += paragraphs[i].get_text().strip('\n')
            if (i > 3):
                break
        item = CrawlerItem()
        item['title'] = soup.title.string
        item['url'] = response.url
        item['url_hash'] = hashlib.sha256(
            item['url'].encode('utf-8')).hexdigest()
        item['text'] = text[:500]

        # do a SPARQL request to DBpedia to get the categories
        sparql = SPARQLWrapper("http://dbpedia.org/sparql")
        sparql.setQuery("""
            SELECT *
            WHERE {<http://dbpedia.org/resource/""" + response.url[30:] + """> <http://purl.org/dc/terms/subject> ?categories .}
        """)
        sparql.setReturnFormat(JSON)
        result = sparql.query().convert()

        categories = []
        for res in result['results']['bindings']:
            categories.append(res['categories']['value'][37:])

        item['categories'] = categories

        links = []
        for link in soup.find_all("a", href=True):
            link = link['href']
            if (link[:6] != "/wiki/" or bool(re.search(':', link[6:]))):
                continue
            link = "https://en.wikipedia.org" + link
            links.append(hashlib.sha256(link.encode('utf-8')).hexdigest())

        item['links'] = {'count': len(links), 'data': links}
        return item

    def parse_item(self, response):
        url = response.url
        if (bool(re.search(':', url[30:]))):
            return

        print('Processing..' + response.url)
        yield self.clean_response(response)
