from scrapy.spiders import CrawlSpider, Rule
from scrapy.linkextractors import LinkExtractor
from crawler.items import CrawlerItem
import hashlib

from bs4 import BeautifulSoup, SoupStrainer

import re


class bcolors:
    HEADER = '\033[95m'
    OKBLUE = '\033[94m'
    OKGREEN = '\033[92m'
    WARNING = '\033[93m'
    FAIL = '\033[91m'
    ENDC = '\033[0m'
    BOLD = '\033[1m'
    UNDERLINE = '\033[4m'


class MediumSpider(CrawlSpider):
    name = 'medium'
    allowed_domains = ["medium.com"]
    start_urls = ['https://medium.com']

    rules = (
        Rule(LinkExtractor(
            allow_domains=('medium.com')
        ),
            callback="parse_item",
            follow=True),
    )

    def clean_response(self, response):
        'Clean here the response and return the item with the clean text'
        soup = BeautifulSoup(response.body, "html5lib")
        divs = soup.find_all("div", class_="section-content")
        text = ""
        for div in divs:
            text += div.get_text() + " "

        item = CrawlerItem()
        item['title'] = soup.title.string
        item['url'] = response.url[:response.url.find('?')]  # remove get parameters
        item['url_hash'] = hashlib.sha256(item['url'].encode('utf-8')).hexdigest()
        item['text'] = text

        links = []
        for link in soup.find_all(href=re.compile(r"https://medium.com/(@|s).*[^/]/.*[$(0-9a-z)]{12}")):
            links.append(hashlib.sha256(link['href'].encode('utf-8')).hexdigest())

        item['links'] = {'count': len(links), 'data': links}
        return item

    def parse_item(self, response):
        url_regex = r"https://medium.com/(@|s).*[^/]/.*[$(0-9a-z)]{12}"

        if re.match(url_regex, response.url):
            print("%s %s %s" % (bcolors.OKGREEN, response.url, bcolors.ENDC))
            yield self.clean_response(response)
        else:
            print("%s %s %s" % (bcolors.FAIL, response.url, bcolors.ENDC))
