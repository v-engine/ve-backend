import pymongo
from bs4 import BeautifulSoup
from scrapy.conf import settings
from scrapy.exceptions import DropItem
from scrapy import log
from nltk.tokenize import word_tokenize


class MongoDBPagesPipeline(object):

    def __init__(self):
        connection = pymongo.MongoClient(
            settings['MONGODB_SERVER'],
            settings['MONGODB_PORT']
        )
        db = connection[settings['MONGODB_DB']]
        self.pages_collection = db[settings['MONGODB_PAGES_COLLECTION']]

    def process_item(self, item, spider):
        # do all the processing here
        self.pages_collection.update_one(
            {
                'url': item['url'],
            },
            {
                "$set": {
                    'title': item['title'],
                    'url': item['url'],
                    'url_hash': item['url_hash'],
                    'text': item['text'],
                    'links': item['links'],
                    'categories': item['categories'],
                }
            },
            upsert=True)
        return item
