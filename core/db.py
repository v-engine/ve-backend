import pymongo


class Database:

    MONGODB_SERVER = "localhost"
    MONGODB_PORT = 27017
    MONGODB_DB = "v-engine"
    MONGODB_PAGES_COLLECTION = "pages"
    MONGODB_BIN_DATA_COLLECTION = "binary-data"
    MONGODB_QUERY_COLLECTION = "queries"
    __instance = None

    def __init__(self):
        if(Database.__instance is not None):
            raise Exception("This class is a singleton")
        connection = pymongo.MongoClient(
            self.MONGODB_SERVER,
            self.MONGODB_PORT
        )
        db = connection[self.MONGODB_DB]
        self.pages_collection = db[self.MONGODB_PAGES_COLLECTION]
        self.bin_data_collection = db[self.MONGODB_BIN_DATA_COLLECTION]
        self.queries_collection = db[self.MONGODB_QUERY_COLLECTION]

    @staticmethod
    def getInstance():
        if(Database.__instance is None):
            Database.__instance = Database()
        return Database.__instance
