# sample.py

import falcon
from falcon_cors import CORS
from apis.document import DocumentAPI
from apis.web_graph import WebGraphLinksAPI
from apis.web_graph import WebGraphNodesAPI
from apis.concept_graph import ConceptGraphAPI
from apis.query import QueryAPI
from apis.query_feedback import QueryFeedbackAPI
from apis.remove_pages import RemovePagesAPI
from apis.pca_graph import PCAGraphAPI
from apis.tsne_graph import TSNEGraphAPI
from apis.radar_graph import RadarGraphAPI

api = falcon.API()

# enable cors
cors = CORS(allow_origins_list=['*'])
public_cors = CORS(allow_all_origins=True,
                   allow_methods_list=['GET', 'POST'],
                   allow_headers_list=["content-type"])
api = falcon.API(middleware=[public_cors.middleware])

# describe apis here
api.add_route('/document/{url_hash}', DocumentAPI())
api.add_route('/query', QueryAPI())
# convention: /concept_graph will always be called on the last issued query
api.add_route('/concept_graph/{hash}/{page}', ConceptGraphAPI())
# convention: /web_graph will always be called on the last issued query
api.add_route('/web_graph/{hash}/links/{page}', WebGraphLinksAPI())
api.add_route('/web_graph/{hash}/nodes/{page}', WebGraphNodesAPI())
api.add_route('/pca_graph/{hash}', PCAGraphAPI())
api.add_route('/tsne_graph/{hash}', TSNEGraphAPI())
api.add_route('/radar_graph', RadarGraphAPI())
api.add_route('/query_feedback/remove_cluster', QueryFeedbackAPI())
api.add_route('/query_feedback/remove_pages', RemovePagesAPI())
