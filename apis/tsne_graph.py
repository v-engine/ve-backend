from core.db import Database
import pymongo
import pickle
import falcon
import sys
import numpy as np
from sklearn.manifold import TSNE


class TSNEGraphAPI:
    def on_get(self, req, res, hash):
        db = Database.getInstance()

        document = db.queries_collection.find_one({'hash': hash})

        if document is None:
            print(" > GET /tsne_graph/hash ERROR - Document not found")
            res.media = []
            res.status = falcon.HTTP_404
            return
        else:
            print(" > Document found")

            '''
            http://scikit-learn.org/stable/modules/generated/sklearn.manifold.TSNE.html
            It is highly recommended to use another dimensionality reduction method 
            (e.g. PCA for dense data or TruncatedSVD for sparse data) to reduce the 
            number of dimensions to a reasonable amount (e.g. 50) if the number of 
            features is very high. This will suppress some noise and speed up the 
            computation of pairwise distances between samples.
            '''

            query_document_indices_bin = document['indices_all']
            query_document_indices = pickle.loads(query_document_indices_bin)
            pages = db.pages_collection.find().sort('_id', pymongo.ASCENDING)

            # we can load it directly from the DB since we saved it previously in /concept_graph
            query_svd_matrix_bin = document['query_svd_matrix']
            query_svd_matrix = pickle.loads(query_svd_matrix_bin)

            # given that TSNE produces always different results, we'll not save it to DB

            tsne_matrix = TSNE(n_components=2).fit_transform(query_svd_matrix)

            concept_graph_array = document['concept_graph']['data']
            n_clusters = document['concept_graph']['n_clusters']
            data = []
            max_weight = -sys.maxsize-1
            min_weight = sys.maxsize

            for i in range(len(concept_graph_array)):

                page = pages[query_document_indices[i]]

                local_max = max(tsne_matrix[i])
                local_min = min(tsne_matrix[i])
                max_weight = local_max if local_max > max_weight else max_weight
                min_weight = local_min if local_min < min_weight else min_weight

                data.append(
                    {
                        'index': i,
                        'url_hash': page['url_hash'],
                        'coordinates': tsne_matrix[i].astype('float64').tolist(),
                        'cluster_index': concept_graph_array[i]['cluster_index']
                    }
                )

            # we need to convert all float32 to float64 since the former is not JSON serializable

            max_weight = max_weight.astype('float64')
            min_weight = min_weight.astype('float64')

            res.media = {'max_weight': max_weight, 'min_weight': min_weight, 'n_clusters': n_clusters, 'data': data}
            res.content_type = falcon.MEDIA_JSON
            res.status = falcon.HTTP_200
