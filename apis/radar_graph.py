import json
import pickle
import pymongo
import falcon
from bson.objectid import ObjectId
from bson.binary import Binary
from core.db import Database

'''
In order to communicate for what pages the frontend needs the relative
weights of the concepts we'll use, if possible, the indices in the array
that is common between both front and back, which is the cluster_graph['data'] array.
When the user sees for example the first 5 pages, front will send to back
0,1,2,3,4. But why not just send min and max? Because of this possible interaction:
instead of just seeing the relative concepts of the only visible pages, the user
has the possibility to select the pages for which he desires to see the concepts so that
he can compare, for example, the first page with the fifth, seventh and so on and
not only a seen range. We'll allow both for the range and also for the 
selective interaction. If front cannot pass to back the indices of the pages,
then we'll modify accordingly the backend to what can be sent.
If front has the possibility, it can directly work on the data received
from /concept_graph to correctly expose the concepts, nevertheless we
create this API to allow more independence between modules
'''


class RadarGraphAPI:
    def on_post(self, req, res):
        req_data = json.loads(req.stream.read())

        '''
        for now we'll receive
        {
            'query_hash': query_hash
            'indices': [0, 3, 5, 6]
        }
        '''

        hash = req_data['query_hash']

        db = Database.getInstance()

        document = db.queries_collection.find_one({'hash': hash})

        if document is None:
            print("> POST /radar_graph ERROR - Document not found")
            res.media = []
            res.status = falcon.HTTP_404
            return
        else:
            print("> Document found")

            indices = req_data['indices']

            concept_graph_array = document['concept_graph']['data']
            n_clusters = document['concept_graph']['n_clusters']

            data = []
            for index in indices:
                axes = []
                concept_weights = concept_graph_array[index]['concept_weights']
                for i in range(n_clusters):
                    axes.append(
                        {
                            "axis": str(i),
                            "value": concept_weights[i],
                            "description": ""
                        }
                    )
                data.append(
                    {
                        "axes": axes,
                        "group": concept_graph_array[index]['title']
                    }
                )

            res.media = {'data': data}
            res.content_type = falcon.MEDIA_JSON
            res.status = falcon.HTTP_200
