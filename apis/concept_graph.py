import pickle
import pymongo
import falcon
import math
import numpy as np
from bson.objectid import ObjectId
from bson.binary import Binary
from core.db import Database
from sklearn.cluster import MeanShift
import sys
from scipy.sparse import csr_matrix, vstack
from sklearn.random_projection import SparseRandomProjection
from sklearn.decomposition import TruncatedSVD
from sklearn.pipeline import make_pipeline
from sklearn.preprocessing import Normalizer

# as a rule of thumb we should always call this API before the other
# ones regarding concepts such as PCA, TSNE, Radar Graph


class ConceptGraphAPI:
    def on_get(self, req, res, hash, page):
        db = Database.getInstance()

        document = db.queries_collection.find_one({'hash': hash})

        if document is None:
            print(" > GET /concept_graph/hash ERROR - Document not found")
            res.media = []
            res.status = falcon.HTTP_404
            return
        else:
            print(" > Document found")

            if 'concept_graph' not in document:

                query_document_indices_bin = document['indices_all']
                query_document_indices = pickle.loads(query_document_indices_bin)

                pages = db.pages_collection.find().sort('_id', pymongo.ASCENDING)

                n_clusters = 5

                # retrieve the query_tf_idf_matrix already computed
                query_tf_idf_matrix_bin = document['query_tf_idf_matrix']
                query_tf_idf_matrix = pickle.loads(query_tf_idf_matrix_bin)

                svd_model = TruncatedSVD(n_components=n_clusters, random_state=50)
                svd_matrix = svd_model.fit_transform(query_tf_idf_matrix)

                # we save the query svd_matrix so that it can be later used by tsne_graph
                db.queries_collection.update_one(
                    {
                        'hash': hash,
                    },
                    {
                        '$set': {
                            'query_svd_matrix': Binary(pickle.dumps(svd_matrix, pickle.HIGHEST_PROTOCOL)),
                        }
                    },
                    upsert=True
                )

                cluster_indices = svd_matrix.argmax(axis=1)

                concept_labels = []
                minimum_maximum_weight = []
                for i in range(n_clusters):
                    concept_labels.append(dict())
                    minimum_maximum_weight.append(sys.maxsize)

                data = []
                max_weight = -sys.maxsize-1
                min_weight = sys.maxsize
                for i in range(len(query_document_indices)):

                    d_page = pages[query_document_indices[i]]

                    cluster_index = np.asscalar(cluster_indices[i])  # since cluster_indices is a matrix
                    local_max = max(svd_matrix[i])
                    local_min = min(svd_matrix[i])
                    max_weight = local_max if local_max > max_weight else max_weight
                    min_weight = local_min if local_min < min_weight else min_weight

                    if (len(concept_labels[cluster_index]) < 3):
                        concept_labels[cluster_index][local_max] = d_page['url_hash']
                        minimum_maximum_weight[cluster_index] = local_max if local_max < minimum_maximum_weight[cluster_index] else minimum_maximum_weight[cluster_index]
                    else:  # we need to control if this local_max is greater than the minimum_maximum_weight
                        if (local_max > minimum_maximum_weight[cluster_index]):  # we have found a better page
                            concept_labels[cluster_index].pop(minimum_maximum_weight[cluster_index])
                            concept_labels[cluster_index][local_max] = d_page['url_hash']

                            # need to find the new minimum_maximum_weight
                            minimum_maximum_weight[cluster_index] = min(concept_labels[cluster_index].keys())

                    data.append({
                        'index': i,
                        'db_index': d_page['index'],
                        'url_hash': d_page['url_hash'],
                        'title': d_page['title'],
                        # this indicates how close/far each page is to a given concept
                        'concept_weights': (svd_matrix[i] + np.ones(n_clusters)).tolist(),
                        'cluster_index': cluster_index
                    })

                max_weight = max_weight + 1
                min_weight = min_weight + 1

                # here we'll add the labels for our clusters
                dimensions = []
                for i in range(n_clusters):
                    dimensions.append(list(concept_labels[i].values()))

                db.queries_collection.update_one(
                    {
                        'hash': hash,
                    },
                    {
                        '$set': {
                            'concept_graph': {
                                'max_weight': max_weight,
                                'min_weight': min_weight,
                                'n_clusters': n_clusters,
                                'data': data,
                                'dimensions': dimensions
                            }
                        }
                    },
                    upsert=True
                )

            else:
                concept_graph = document['concept_graph']

                max_weight = concept_graph['max_weight']
                min_weight = concept_graph['min_weight']
                n_clusters = concept_graph['n_clusters']
                data = concept_graph['data']
                dimensions = concept_graph['dimensions']

            # paging
            items_per_page = 100
            needed_pages = math.ceil(len(data) / items_per_page)
            print(page)
            page = int(page)

            if (page >= needed_pages):
                res.status = falcon.HTTP_400
                return

            content = {
                'max_weight': max_weight,
                'min_weight': min_weight,
                'n_clusters': n_clusters,
                'data': data[page*items_per_page:(page*items_per_page)+items_per_page],
                'dimensions': dimensions,
                "pages": needed_pages
            }

            res.media = content
            res.content_type = falcon.MEDIA_JSON
            res.status = falcon.HTTP_200
