import json
import pickle
import pymongo
import falcon
import hashlib
from bson.objectid import ObjectId
from bson.binary import Binary
from core.db import Database
from nltk.tokenize import word_tokenize
from sklearn.feature_extraction.text import TfidfVectorizer
from sklearn.metrics.pairwise import cosine_similarity
from scipy.sparse import csr_matrix, vstack
from numpy import argsort, flipud


class QueryAPI:
    def on_get(self, req, res):
        if("q" not in req.params):
            print("QueryAPI [GET] Error - No query specified")
            res.media = []
            res.status = falcon.HTTP_400
            return

        db = Database.getInstance()

        query_req = req.params['q']
        query_hash = hashlib.sha256(query_req.encode('utf-8')).hexdigest()

        document = db.queries_collection.find_one({'hash': query_hash})

        if(document is None):
            tf_idf_model_bin = db.bin_data_collection.find_one({'name': 'tf_idf_model'})
            tf_idf_model = pickle.loads(tf_idf_model_bin['data'])

            tf_idf_matrix_bin = db.bin_data_collection.find_one({'name': 'tf_idf_matrix'})
            tf_idf_matrix = pickle.loads(tf_idf_matrix_bin['data'])

            query = tf_idf_model.transform([query_req])

            similarity_weights = [item[0] for item in cosine_similarity(tf_idf_matrix, query)]

            results = []
            for similarity_weight in similarity_weights:
                if similarity_weight <= 0.001:
                    break
                results.append(similarity_weight)

            indices_all = flipud(argsort(similarity_weights)).tolist()
            # Here se save only the first 150 query entries
            indices_all = indices_all[:150]
            indices = indices_all[:10]

            # query tf_idf_matrix will later be used by concept_graph, PCA_graph
            query_tf_idf_matrix = None  # otherwise we would have a matrix with the first row initialized to 0
            for index in indices_all:
                query_tf_idf_matrix = vstack([query_tf_idf_matrix, tf_idf_matrix[index]], format="csr")

            print("> Saving query to DB")

            db.queries_collection.update_one(
                {
                    'hash': query_hash,
                },
                {
                    '$set': {
                        'query_tf_idf_matrix': Binary(pickle.dumps(query_tf_idf_matrix, pickle.HIGHEST_PROTOCOL)),
                        'vector_representation': Binary(pickle.dumps(query, pickle.HIGHEST_PROTOCOL)),
                        'indices': Binary(pickle.dumps(indices, pickle.HIGHEST_PROTOCOL)),
                        'indices_all': Binary(pickle.dumps(indices_all, pickle.HIGHEST_PROTOCOL))
                    }
                },
                upsert=True
            )

        else:
            print("> Retrieving query from DB")
            indices = pickle.loads(document['indices'])
            indices_all = pickle.loads(document['indices_all'])

        pages = db.pages_collection.find().sort('_id', pymongo.ASCENDING)
        similar_pages = []
        for index in indices_all:
            similar_pages.append(pages[index])
        result = {'hash': query_hash}

        data = []
        for page in similar_pages:
            data.append(
                {
                    'title': page['title'],
                    'url': page['url'],
                    'url_hash': page['url_hash'],
                    'excerpt': page['text'][:300]
                }
            )
        result['data'] = data
        result['total_results'] = len(indices_all)
        print("QueryAPI [GET] Success for '" + req.params['q'] + "'")
        res.media = result
        res.content_type = falcon.MEDIA_JSON
        res.status = falcon.HTTP_200
