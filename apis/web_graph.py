from core.db import Database
import pymongo
import pickle
import falcon
import hashlib
import networkx as nx
import sys
import math


class WebGraphNodesAPI:
    def on_get(self, req, res, hash, page):
        db = Database.getInstance()

        document = db.queries_collection.find_one({'hash': hash})

        if document is None:
            print(" > GET /web_graph/hash ERROR - Document not found")
            res.media = []
            res.status = falcon.HTTP_404
            return
        else:
            print(" > Document found")

            if 'web_graph' not in document:

                query_document_indices_bin = document['indices_all']
                query_document_indices = pickle.loads(query_document_indices_bin)

                pages = db.pages_collection.find().sort('_id', pymongo.ASCENDING)

                nodes = []
                links = []
                urls = []
                for index in query_document_indices:
                    urls.append(pages[index]['url_hash'])
                    nodes.append({
                        'url_hash': pages[index]['url_hash']
                    })

                j = 0
                for index in query_document_indices:
                    j = 0
                    for link in pages[index]['links']['data']:
                        j += 1
                        if(link in urls):
                            links.append({
                                'source': pages[index]['url_hash'],
                                'target': link
                            })
                        if j > 30:
                            break

                # PageRank
                G = nx.DiGraph()
                for node in nodes:
                    G.add_node(node['url_hash'])
                for link in links:
                    G.add_edge(link['source'], link['target'])
                weights = nx.pagerank(G)

                nodes_with_weights = []
                for node in nodes:
                    nodes_with_weights.append({
                        'url_hash': node['url_hash'],
                        'weight': weights[node['url_hash']]
                    })

                max_weight = -sys.maxsize-1
                min_weight = sys.maxsize

                for weight in weights.values():
                    max_weight = weight if weight > max_weight else max_weight
                    min_weight = weight if weight < min_weight else min_weight

                weights = {}
                weights['min_weight'] = min_weight
                weights['max_weight'] = max_weight

                db.queries_collection.update_one(
                    {
                        'hash': hash,
                    },
                    {
                        '$set': {
                            'web_graph': {
                                'nodes': nodes_with_weights,
                                'links': links,
                                'weights': weights
                            }
                        }
                    },
                    upsert=True
                )

            else:
                web_graph = document['web_graph']
                nodes_with_weights = web_graph['nodes']
                links = web_graph['links']
                weights = web_graph['weights']

            # paging
            items_per_page = 100
            needed_pages = math.ceil(len(nodes_with_weights) / items_per_page)
            page = int(page)

            if (page >= needed_pages):
                res.status = falcon.HTTP_400
                return

            content = {
                'weights': weights,
                'nodes': nodes_with_weights[page*items_per_page:(page*items_per_page) + items_per_page],
                'pages': needed_pages
            }

            res.media = content
            res.content_type = falcon.MEDIA_JSON
            res.status = falcon.HTTP_200


class WebGraphLinksAPI:
    def on_get(self, req, res, hash, page):
        db = Database.getInstance()

        document = db.queries_collection.find_one({'hash': hash})

        if document is None:
            print(" > GET /web_graph/hash ERROR - Document not found")
            res.media = []
            res.status = falcon.HTTP_404
            return
        else:
            print(" > Document found")

            if 'web_graph' not in document:

                print(" > GET /web_graph/links/hash/page ERROR - Request nodes before links")
                res.media = []
                res.status = falcon.HTTP_400
                return

            else:
                web_graph = document['web_graph']
                links = web_graph['links']
                weights = web_graph['weights']

            # paging
            items_per_page = 200
            needed_pages = math.ceil(len(links) / items_per_page)
            page = int(page)

            if (page >= needed_pages):
                res.status = falcon.HTTP_400
                return

            content = {
                'weights': weights,
                'links': links[page*items_per_page:(page*items_per_page) + items_per_page],
                'pages': needed_pages
            }

            res.media = content
            res.content_type = falcon.MEDIA_JSON
            res.status = falcon.HTTP_200
