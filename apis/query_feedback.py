import json
import pickle
import pymongo
import hashlib
import falcon
from bson.binary import Binary
from scipy.sparse import csr_matrix, vstack
import numpy as np
from numpy import argsort, flipud
from core.db import Database
from sklearn.metrics.pairwise import cosine_similarity


class QueryFeedbackAPI:
    def on_post(self, req, res):
        # query_hash, cluster_index
        req_data = json.loads(req.stream.read())
        query_hash = req_data['query_hash']
        cluster_index = req_data['cluster_index']

        ALPHA = 1
        BETA = 0.8
        GAMMA = 0.1  # numbers according to Wikipedia, GAMMA could also be 0

        db = Database.getInstance()

        tf_idf_matrix_bin = db.bin_data_collection.find_one({'name': 'tf_idf_matrix'})
        tf_idf_matrix = pickle.loads(tf_idf_matrix_bin['data'])

        query_document = db.queries_collection.find_one({'hash': query_hash})
        concept_graph = query_document['concept_graph']['data']

        query_vector_representation = pickle.loads(query_document['vector_representation'])

        relevant_matrix = None  # otherwise we would have a matrix with the first row initialized to 0
        relevant_indices = []

        for document in concept_graph:
            if document['cluster_index'] != cluster_index:
                relevant_matrix = vstack([relevant_matrix, tf_idf_matrix[document['db_index']]], format="csr")
                relevant_indices.append(document['db_index'])

        mask = np.ones(tf_idf_matrix.shape[0], dtype=bool)
        mask[relevant_indices] = False

        irrelevant_matrix = tf_idf_matrix[mask]

        query_optimized = ALPHA * query_vector_representation + BETA * \
            relevant_matrix.mean(axis=0) - GAMMA * irrelevant_matrix.mean(axis=0)

        query_optimized_compressed = csr_matrix(query_optimized)
        query_hash = hashlib.sha256(str(query_optimized_compressed).encode('utf-8')).hexdigest()

        similarity_weights = [item[0] for item in cosine_similarity(tf_idf_matrix, query_optimized_compressed)]

        results = []
        for similarity_weight in similarity_weights:
            if similarity_weight <= 0.001:
                break
            results.append(similarity_weight)

        indices_all = flipud(argsort(similarity_weights)).tolist()
        # Here se save only the first 150 query entries
        indices_all = indices_all[:150]
        indices = indices_all[:10]

        # query tf_idf_matrix will later be used by concept_graph, PCA_graph
        query_tf_idf_matrix = None  # otherwise we would have a matrix with the first row initialized to 0
        for index in indices_all:
            query_tf_idf_matrix = vstack([query_tf_idf_matrix, tf_idf_matrix[index]], format="csr")

        print("> Saving query to DB")

        db.queries_collection.update_one(
            {
                'hash': query_hash,
            },
            {
                '$set': {
                    'query_tf_idf_matrix': Binary(pickle.dumps(query_tf_idf_matrix, pickle.HIGHEST_PROTOCOL)),
                    'vector_representation': Binary(pickle.dumps(query_optimized_compressed, pickle.HIGHEST_PROTOCOL)),
                    'indices': Binary(pickle.dumps(indices, pickle.HIGHEST_PROTOCOL)),
                    'indices_all': Binary(pickle.dumps(indices_all, pickle.HIGHEST_PROTOCOL))
                }
            },
            upsert=True
        )

        pages = db.pages_collection.find().sort('_id', pymongo.ASCENDING)
        similar_pages = []
        for index in indices_all:
            similar_pages.append(pages[index])
        result = {'hash': query_hash}

        data = []
        for page in similar_pages:
            data.append(
                {
                    'title': page['title'],
                    'url': page['url'],
                    'excerpt': page['text'][:300]
                }
            )
        result['data'] = data
        result['total_results'] = len(indices_all)

        print("> QueryFeedbackAPI[POST] Success")

        res.media = result
        res.content_type = falcon.MEDIA_JSON
        res.status = falcon.HTTP_200
