from core.db import Database
from bson.objectid import ObjectId
import falcon


class DocumentAPI:
    def on_get(self, req, res, url_hash):
        db = Database.getInstance()

        document = db.pages_collection.find_one({'url_hash': url_hash})
        if document is None:
            print("QueryAPI [GET] Error - Document not found")
            res.media = []
            res.status = falcon.HTTP_404
            return

        res.media = {
            'title': document['title'],
            'url': document['url'],
            'excerpt': document['text'][:300]
        }
