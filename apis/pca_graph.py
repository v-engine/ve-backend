from core.db import Database
import numpy as np
from sklearn.decomposition import PCA
import pymongo
import pickle
import falcon
import sys
from sklearn.pipeline import make_pipeline
from sklearn.preprocessing import Normalizer


class PCAGraphAPI:
    def on_get(self, req, res, hash):
        db = Database.getInstance()

        document = db.queries_collection.find_one({'hash': hash})

        if document is None:
            print("> GET /pca_graph/hash ERROR - Document not found")
            res.media = []
            res.status = falcon.HTTP_404
            return

        else:
            print("> Document found")

            if 'pca_graph' not in document:

                query_tf_idf_matrix_bin = document['query_tf_idf_matrix']
                query_tf_idf_matrix = pickle.loads(query_tf_idf_matrix_bin)

                query_document_indices_bin = document['indices_all']
                query_document_indices = pickle.loads(query_document_indices_bin)

                pages = db.pages_collection.find().sort('_id', pymongo.ASCENDING)

                pca_model = PCA(n_components=2)
                pca_matrix = pca_model.fit_transform(query_tf_idf_matrix.todense())

                # if we assume that the call to /concept_graph
                # occurs before the call to /pca_graph
                # then I can retrieve the saved cluster_index
                # for each page

                concept_graph_array = document['concept_graph']['data']
                n_clusters = document['concept_graph']['n_clusters']
                data = []
                max_weight = -sys.maxsize-1
                min_weight = sys.maxsize

                for i in range(len(concept_graph_array)):

                    page = pages[query_document_indices[i]]

                    local_max = max(pca_matrix[i])
                    local_min = min(pca_matrix[i])
                    max_weight = local_max if local_max > max_weight else max_weight
                    min_weight = local_min if local_min < min_weight else min_weight

                    data.append(
                        {
                            'index': i,
                            'url_hash': page['url_hash'],
                            'coordinates': pca_matrix[i].tolist(),
                            'cluster_index': concept_graph_array[i]['cluster_index']
                        }
                    )

                db.queries_collection.update_one(
                    {
                        'hash': hash,
                    },
                    {
                        '$set': {
                            'pca_graph': {
                                'max_weight': max_weight,
                                'min_weight': min_weight,
                                'n_clusters': n_clusters,
                                'data': data
                            }
                        }
                    },
                    upsert=True
                )

            else:
                pca_graph = document['pca_graph']

                max_weight = pca_graph['max_weight']
                min_weight = pca_graph['min_weight']
                n_clusters = pca_graph['n_clusters']
                data = pca_graph['data']

            res.media = {'max_weight': max_weight, 'min_weight': min_weight, 'n_clusters': n_clusters, 'data': data}
            res.content_type = falcon.MEDIA_JSON
            res.status = falcon.HTTP_200
