#!/bin/bash
echo "==> Installing virtualenv.."
virtualenv --system-site-packages -p python3 env
source env/bin/activate
echo "==> Installing packages.."
pip install -r requirements.txt
echo "==> Installing deps.."
python -m nltk.downloader -d env/nltk_data punkt
echo "==> Enable the env with command: source env/bin/activate"